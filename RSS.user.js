// ==UserScript==
// @name        RSS
// @description Insert feed icon on pages with RSS/Atom feeds
// @version     1
// @grant       none
// ==/UserScript==

/* SPDX-License-Identifier: Zlib
   Copyright (C) 2022 smpl <smpl@slamkode.ml>
   Embedding RSS icon under LGPL */

function insert_feed_icon(feed) {
  var s_icon = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgICAgaWQ9IlJTU2ljb24iCiAgICAgdmlld0JveD0iMCAwIDggOCIgd2lkdGg9IjI1NiIgaGVpZ2h0PSIyNTYiPgoKICA8dGl0bGU+UlNTIGZlZWQgaWNvbjwvdGl0bGU+CgogIDxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+CiAgICAuYnV0dG9uIHtzdHJva2U6IG5vbmU7IGZpbGw6IG9yYW5nZTt9CiAgICAuc3ltYm9sIHtzdHJva2U6IG5vbmU7IGZpbGw6IHdoaXRlO30KICA8L3N0eWxlPgoKICA8cmVjdCAgIGNsYXNzPSJidXR0b24iIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHJ4PSIxLjUiIC8+CiAgPGNpcmNsZSBjbGFzcz0ic3ltYm9sIiBjeD0iMiIgY3k9IjYiIHI9IjEiIC8+CiAgPHBhdGggICBjbGFzcz0ic3ltYm9sIiBkPSJtIDEsNCBhIDMsMyAwIDAgMSAzLDMgaCAxIGEgNCw0IDAgMCAwIC00LC00IHoiIC8+CiAgPHBhdGggICBjbGFzcz0ic3ltYm9sIiBkPSJtIDEsMiBhIDUsNSAwIDAgMSA1LDUgaCAxIGEgNiw2IDAgMCAwIC02LC02IHoiIC8+Cgo8L3N2Zz4=';
  var el_div = document.createElement('div');
  el_div.setAttribute('style', 'position: fixed; z-index: 1000; right: 8px; bottom: 8px; width: 16px; height: 16px; margin: 0; padding: 0;');
  el_div.innerHTML = el_div.innerHTML + ' <a href="' + feed + '"><img src="' + s_icon + '" width="16px" height="16px" alt="feed" style="width: 16px; height: 16px; margin: 0; padding: 0;"></a> ';
  document.body.appendChild(el_div);
}

var rss = document.querySelector("html head link[type='application/rss+xml']");
var atom = document.querySelector("html head link[type='application/atom+xml']");

// Prefers RSS over Atom
if(rss)
  insert_feed_icon(rss.getAttribute('href'));
else if(atom)
  insert_feed_icon(atom.getAttribute('href'));

// ==UserScript==
// @name        Altinget NoJS
// @description Altinget NoJS
// @include     https://www.altinget.dk/*
// @version     1
// @grant       none
// ==/UserScript==

/* SPDX-License-Identifier: Zlib
 * Copyright (C) 2021 smpl <smpl@slamkode.ml> */

var article_text;
var article_title;
var article_image;
var article_style;
var container;
var elements;

/* Hent titel og artikelbillede fra OpenGraph data. */
article_title = document.head.querySelector('meta[property="og:title"]').getAttribute('content');
article_image = document.head.querySelector('meta[property="og:image"]').getAttribute('content');

/* Fjern alle script, style og link tags fra <head> */
elements = document.head.querySelectorAll('script, link, style');
elements.forEach(
	function(e) {
		e.parentNode.removeChild(e);
	}
);

/* Find alle script tags på siden og led efter JSON med indhold. */
elements = document.querySelectorAll('script');
for(let i=0; i < elements.length; i++) {
	if(/window.__NUXT__/.test(elements[i].innerHTML)) {
		article_text = JSON.parse('"' + elements[i].innerHTML.match(/,Text:"(.*?)(?<!\\)"/)[1] + '"');
		break;
	}
}

/* Erstat alt indhold i body med en tom kontainer <div> */
document.body.innerHTML = '<div id="container"></div>';
container = document.getElementById('container');

/* Indsæt et <style> element i <head> og sæt maks. bredde for 
 * indholdskontaineren */
article_style = document.createElement('style');
article_style.innerHTML = '#container { max-width: 640px };';
document.head.appendChild(article_style);

/* Indsæt overskrift, billede og tekst i container */
container.innerHTML = 
	'<h1>' + article_title + '</h1>' +
	'<img width="600px" src="' + article_image + '">' +
	'<div id="article">' + article_text + '</div>';

// ==UserScript==
// @name        RSSonApplePod
// @description Add link to RSS feed on podcasts.apple.com
// @include     https://podcasts.apple.com/*
// @version     1
// @grant       GM.xmlHttpRequest
// ==/UserScript==

/* SPDX-License-Identifier: Zlib
   Copyright (C) 2020 smpl <smpl@slamkode.ml>
   Embedding RSS icon under LGPL */

function insert_feed_icon()
{
  var e_meta_id = e_head.querySelector('meta[name="apple:content_id"]');
  
  if(!e_meta_id)
    return e_meta_id;
  
  var e_insert = document.querySelector('span.product-header__title');
  
  if(!e_insert)
    return e_insert;
  
  var s_meta_url = 'https://itunes.apple.com/lookup?id=' + e_meta_id.getAttribute('content') + '&entity=podcast';
  var s_icon = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgICAgaWQ9IlJTU2ljb24iCiAgICAgdmlld0JveD0iMCAwIDggOCIgd2lkdGg9IjI1NiIgaGVpZ2h0PSIyNTYiPgoKICA8dGl0bGU+UlNTIGZlZWQgaWNvbjwvdGl0bGU+CgogIDxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+CiAgICAuYnV0dG9uIHtzdHJva2U6IG5vbmU7IGZpbGw6IG9yYW5nZTt9CiAgICAuc3ltYm9sIHtzdHJva2U6IG5vbmU7IGZpbGw6IHdoaXRlO30KICA8L3N0eWxlPgoKICA8cmVjdCAgIGNsYXNzPSJidXR0b24iIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHJ4PSIxLjUiIC8+CiAgPGNpcmNsZSBjbGFzcz0ic3ltYm9sIiBjeD0iMiIgY3k9IjYiIHI9IjEiIC8+CiAgPHBhdGggICBjbGFzcz0ic3ltYm9sIiBkPSJtIDEsNCBhIDMsMyAwIDAgMSAzLDMgaCAxIGEgNCw0IDAgMCAwIC00LC00IHoiIC8+CiAgPHBhdGggICBjbGFzcz0ic3ltYm9sIiBkPSJtIDEsMiBhIDUsNSAwIDAgMSA1LDUgaCAxIGEgNiw2IDAgMCAwIC02LC02IHoiIC8+Cgo8L3N2Zz4=';

  GM.xmlHttpRequest({
    method: "GET",
    url: s_meta_url,
    onload: function(response) {
      var s_meta_data = response.responseText;
      var s_feed_url = JSON.parse(s_meta_data).results[0].feedUrl;
      e_insert.innerHTML = e_insert.innerHTML + ' <a href="' + s_feed_url + '"><img src="' + s_icon + '" alt="feed" style="width: 16px; height: 16px; display: inline-block;"></a> ';
    }
  });
}

var e_head = document.querySelector('head');

// check if this page is a show and not an episode
if( e_head.querySelector('script[name="schema:podcast-show"]') )
  insert_feed_icon();

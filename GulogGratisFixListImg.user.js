// ==UserScript==
// @name		GulogGratisFixListImg
// @description	Fiks afhængighed af eksterne JS biblioteker
// @include		https://www.guloggratis.dk/*
// @version		1
// @grant		none
// ==/UserScript==

/* SPDX-License-Identifier: Zlib
 * Copyright (C) 2021 smpl <smpl@slamkode.ml> */
   
/* Hvis man browser guloggratis.dk med nedenstående tilladelser i 
 * uMatrix og alt andet blokeret, så vises der ikke billeder i 
 * annoncelister/søgninger.
 *
 *	* * * block
 *	guloggratis.dk 1st-party css allow
 *	guloggratis.dk 1st-party frame allow
 *	guloggratis.dk 1st-party image allow
 *	guloggratis.dk 1st-party fetch allow
 *	guloggratis.dk 1st-party script allow
 *
 * Dette brugerscript viser billederne.
*/

var listing = document.querySelector('div[data-inspect="search-result:listings"]');
var images = listing.querySelectorAll('img[data-cfsrc]');

images.forEach(
	function(e) {
		e.setAttribute('src', e.getAttribute('data-cfsrc'));
		e.removeAttribute('data-cfsrc');
		e.style.removeProperty('display');
		e.style.removeProperty('visibility');
	}
);
